# pyberries.data.get_histogram

```python
hist = get_histogram(df_in, col, binsize=1, binlabels='center', density=False, groupby='Dataset', discrete=False)
```

Compute and return a histogram for the specified column in the input DataFrame.

## Parameters

### **df_in**: *pandas DataFrame*
DataFrame that contains the data. Example: `data.Bacteria`

### **col**: *string*
Name of the column from which the histogram should be made

### **binsize**: *float*
Size of the histogram bins. Value is not used if `discrete=True`.

### **binlabels**: *string*
Determines how continuous bins will be labeled. Options are:
- center (default): return the middle value of each bin
- left: return the lower bound of each bin
- right: return the upper bound of each bin
Value is not used if `discrete=True`.

### **density**: *bool*
Set to True to normalise each histogram bin by the total number of observations, to False (default) to return the raw count for each bin

### **groupby**: *string or list of strings*
Columns to use to group the data. One histogram (normalised independently) will be made for each group. The groupby columns will also be present in the resulting table.

### **discrete**: *bool*
Set to True if the data in *col* is discrete, and to False if it is continuous.


## Returns

### **hist**: *pandas DataFrame*
Computed histogram, with the following columns:
- Any grouping columns provided in *groupby*
- *col* (name of the original column): bin values. In the case of a non-discrete histogram, contains the value determined by *binlabels*.
- *count* (if density=False) or *proportion* (if density=True): count (or normalised count) of values for the corresponding bin


## Examples
Make a normalised cell length histogram for each group:
```python
from pyberries.data import get_histogram

hist = get_histogram(data.Bacteria, col='SpineLength', binsize=0.2, density=True, groupby='Group')
```

Make a spot count histogram for each position:
```python
from pyberries.data import get_histogram

hist = get_histogram(data.Bacteria, col='SpotCount', discrete=True, density=True, groupby=['Group', 'Dataset', 'Position'])
```
Note that *groupby* contains `Dataset` so the same positions from different datasets are not counted together; and `Group`, so the column in retained in the output histogram, and can be used to group datasets in a figure.
