# pyberries.data.DatasetPool.add_from_parent

```python
DatasetPool.add_from_parent(object_name, col=[], inplace=False)
```

Add a column value from a parent object to the specified object based on indices. For example, child objects '0-1-0' and '0-1-1' will both receive the same parent value from '0-1'. The parent table to use is inferred from the Bacmman config file.

## Parameters

### **object_name**: *string*
Object table to be processed

### **col**: *list of strings*
Name of the columns to be added to *object_name*

### **inplace**: *bool*
Set to True to modify the DatasetPool in place, to False (default) to return the modified DatasetPool


## Examples
```python
data.add_from_parent(object_name='Bacteria', col=['SpineLength', 'SpineWidth'], inplace=True)
```