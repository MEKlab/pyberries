# pyberries.data.DatasetPool.filter_parent

```python
DatasetPool.filter_parent(source, inplace=False)
```

Filter out objects from a parent table if they are not contained in the child table(s) specified in *source*.

Note: this function is *not* applied systematically when calling `apply_filters` or creating a DatasetPool. It must be called on its own.

## Parameters

### **source**: *string or list of strings*
Name of the child objects to use as a reference

### **inplace**: *bool*
Set to True to modify the DatasetPool in place, to False (default) to return the modified DatasetPool


## Examples
```python
data.filter_parent(source='Spot_detection', inplace=True)
```