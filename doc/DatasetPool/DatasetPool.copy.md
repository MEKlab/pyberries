# pyberries.data.DatasetPool.copy

```python
DatasetPool.copy()
```

Copy a DatasetPool

## Parameters

None

## Returns

DatasetPool

## Examples
```python
data_2 = data.copy()
```
