# pyberries.data.DatasetPool.fuse_columns

```python
DatasetPool.fuse_columns(object_name, columns:list=[], new:str='new_col', delimiter:str='-', inplace=False)
```

Creates a new column in the specified table that fuses the contents of one or several columns, separated by the specified delimiter (defaults to "-"). The resulting column will contain strings. Fused columns are left unchanged.

## Parameters

### **object_name**: *string*
Object table to be processed

### **columns**: *list*
List of names of columns to be fused

### **new**: *string*
Name of the column to be created (defaults to 'new_col')

### **delimiter**: *string*
Delimiter to be placed between the data of the fused columns

### **inplace**: *bool*
Set to True to modify the DatasetPool in place, to False (default) to return the modified DatasetPool


## Examples
```python
data.fuse_columns(object_name='Spot_detection', columns=['Frame','Idx'], new='Indices', delimiter='-', inplace=True)
```
