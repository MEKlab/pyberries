# pyberries.data.DatasetPool.bin_column

```python
DatasetPool.bin_column(col, binsize=1, binlabels='center', object_name=None, inplace=False)
```

Bins the specified columns, and for each value assigns the middle value of its corresponding bin.

A new column will be added to the target tables, with name *col*_bin.

## Parameters

### **col**: *string or list of strings*
Columns in target tables to be processed

### **binsize**: *int or float*
Size of the bins

### **binlabels**: *string*
Determines how continuous bins will be labeled. Options are:
- center (default): return the middle value of each bin
- left: return the lower bound of each bin
- right: return the upper bound of each bin

### **object_name**: *string or list of strings*
Object(s) to be processed

### **inplace**: *bool*
Set to True to modify the DatasetPool in place, to False (default) to return the modified DatasetPool

## Examples
Bin the acquisition time (`Time_min`) in 20 min intervals:
```python
data = data.bin_column(col='Time_min', binsize=20, object_name='Bacteria')
```
