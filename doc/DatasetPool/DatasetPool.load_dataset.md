# pyberries.data.DatasetPool.load_dataset

```python
DatasetPool.load_dataset(ds_path, ds, grp, preprocessing, inplace=False)
```

Adds the measurement tables from specified dataset to the DatasetPool.

Note: this is called on each item of `dsList` when creating a new DatasetPool.

## Parameters

### **ds_path**: *string*
Path to the dataset folder

### **ds**: *string*
Dataset name

### **grp**: *string*
Dataset group

### **preprocessing**: *dict*
Pre-processing to be applied to the measurement tables before adding them to the DatasetPool, in the format {*object_name*:*function*}

### **inplace**: *bool*
Set to True to modify the DatasetPool in place, to False (default) to return the modified DatasetPool

## Examples
```python
data.load_dataset(dsPath='C:\\Downloads\\BACMMAN', ds='220203_cipro0', grp='No cipro', inplace=True)
```