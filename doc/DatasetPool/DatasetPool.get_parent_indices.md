# pyberries.data.DatasetPool.get_parent_indices

```python
DatasetPool.get_parent_indices(obj, indices='Indices', newcol='ParentIndices', inplace=False)
```

Creates a new column in the specified table that contains the indices of the parent object. For example, the object '0-1-2' has '0-1' as parent. Typically, if '0-1-2' is a spot, '0-1' would be the cell that contains this spot and '0' is the frame that contains the cell.

## Parameters

### **obj**: *string*
Object table from which to retrieve the parent indices

### **indices**: *string*
Name of the column containing the indices (defaults to 'Indices')

### **newcol**: *string*
Name of the column to be created with the parent indices (defaults to 'ParentIndices')

### **inplace**: *bool*
Set to True to modify the DatasetPool in place, to False (default) to return the modified DatasetPool


## Examples
```python
data.get_parent_indices(obj='Spot_detection', inplace=True)
```