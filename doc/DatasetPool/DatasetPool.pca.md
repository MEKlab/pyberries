# pyberries.data.DatasetPool.pca

```python
DatasetPool.pca(include, n_components=2, scaling=True, object_name=None, inplace=False)
```

Runs a Principal Component Analysis (PCA) on the columns specified in "include". Adds as many columns as PCA components (default 2)

## Parameters

### **include**: *list of strings*
Column names to be included in the principal component analysis

### **n_components**: *int*
Number of PCA components to output

### **scaling**: *bool*
Whether to scale (normalise) the different features prior to PCA

### **object_name**: *string or list of strings*
Object(s) to be processed

### **inplace**: *bool*
Set to True to modify the DatasetPool in place, to False (default) to return the modified DatasetPool

## Examples
```python
data = data.pca(object_name='Bacteria', include=['SpineLength', 'SpineWidth', 'FluoIntensity'], n_components=2)
```
