# pyberries.data.DatasetPool.get_idx

```python
DatasetPool.get_idx(obj, idx=0, indices='Indices', newcol='ParentIdx', inplace=False)
```

Creates a new column in the specified table that contains the n-th index of the object. Be aware that numbering starts at 0. For example, the 0-th index of the object '0-1-2' is 0, and the 1st index is 1.

## Parameters

### **obj**: *string*
Object table to be processed

### **idx**: *int*
Position of the index to be extracted (numbering starts at 0)

### **indices**: *string*
Name of the column containing the indices (defaults to 'Indices')

### **newcol**: *string*
Name of the column to be created with the parent indices (defaults to 'ParentIdx')

### **inplace**: *bool*
Set to True to modify the DatasetPool in place, to False (default) to return the modified DatasetPool


## Examples
```python
data.get_idx(obj='Spot_detection', idx=1, inplace=True)
```