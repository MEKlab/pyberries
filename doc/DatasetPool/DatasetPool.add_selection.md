# pyberries.data.DatasetPool.add_selection

```python
DatasetPool.add_selection(selection_name=None, inplace=False)
```

Add one or several columns with selection information to their corresponding table.

## Parameters

### **selection_name**: *string* or *list of strings*
Name of the selection(s) to import. If not provided, all available selections will be imported.

### **inplace**: *bool*
Set to True to modify the DatasetPool in place, to False (default) to return the modified DatasetPool

## Examples
```python
data.add_selection(inplace=True)
```
