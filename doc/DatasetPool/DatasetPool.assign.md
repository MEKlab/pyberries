# pyberries.data.DatasetPool.assign

```python
DatasetPool.assign(object_name=None, inplace=False, **kwargs)
```

Use pandas' `DataFrame.assign` method to add columns to the specified object tables.

For more example on the use of assign, see the the [pandas documentation](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.assign.html).

## Parameters

### **object_name**: *string or list of strings*
Object(s) to be processed

### **inplace**: *bool*
Set to True to modify the DatasetPool in place, to False (default) to return the modified DatasetPool

### ****kwargs**
Assignments to create new columns, as accepted by pandas' assign function.

## Examples
Normalise number of spots by SpineLength (norm_spots will be the name of the newly created column):
```python
data = data.assign(object_name='Bacteria', norm_spots=lambda df: df.SpotCount / df.SpineLength)
```

Create a boolean column to detect if the length/width ratio of cells is superior to 5:
```python
data = data.assign(object_name='Bacteria', filamentous_cells=lambda df: (df.SpineLength/df.SpineWidth) > 5)
```

Several assignments can be performed within the same call to `assign`, and the later ones can use columns that were previously created:
```python
data = data.assign(object_name='Spot_detection',
                   spot_pos=lambda df: df.SpotCurvilinearCoord / df.SpineLength,
                   is_centered=lambda df: (spot_pos > 0.25) & (spot_pos < 0.75)
                   )
```

To perform different assignments on different object tables, method chaining can be used:
```python
data = (data
        .assign(object_name='Bacteria', norm_spots=lambda df: df.SpotCount / df.SpineLength)
        .assign(object_name='Spot_detection', spot_pos=lambda df: df.SpotCurvilinearCoord / df.SpineLength - 0.5)
        )
```
