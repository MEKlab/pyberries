# pyberries.data.DatasetPool.heatmap_metrics

```python
DatasetPool.heatmap_metrics(object_name=None, inplace=False)
```

Adds normalised spot positions along long and short axis (`normLongCoord`, `normShortCoord`) and "centered" positions (where 0 is the center of the cell) without normalisation (`centerLongCoord`).

Note: the `SpineRadialCoord` column from Bacmman is already centered, so we don't need to create a "centerShortCoord" column.

## Parameters

### **object_name**: *string or list of strings*
Object(s) to be processed

### **inplace**: *bool*
Set to True to modify the DatasetPool in place, to False (default) to return the modified DatasetPool

## Examples
```python
data = data.heatmap_metrics(object_name='CFP_spots')
```
