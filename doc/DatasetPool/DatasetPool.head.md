# pyberries.data.DatasetPool.head

```python
DatasetPool.head(nlines=5)
```

Display the first few lines of every data table contained in the DatasetPool (analogous to pandas [DataFrame.head](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.head.html)).

## Parameters

### **nlines**: *int*
Number of lines to display for each table (default 5).

## Example
```python
data.head(5)
```
