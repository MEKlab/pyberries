# pyberries.data.DatasetPool.plot_preset

```python
DatasetPool.plot_preset(preset, object_name, return_axes=False, **kwargs)
```

Make a pre-defined plot from an object table.

## Parameters

### **preset**: *string*
Type of plot to use from the following list:
- histogram
- bar
- line
- line_fit
- histogram_fit
- scatter
- boxplot
- datapoints_and_mean
- heatmap
- timeseries
- boxenplot
- spot_tracks
- rates_summary
- grey_lines_and_highlight

### **object_name**: *string*
Name of the object table to plot from

### **return_axes**: *boolean*
Return a figure axes object, that can be used for further modification (adding more plot elements, moving the legend,...). Defaults to False.

### ****kwargs**: *dictionary*
Dictionary of arguments that will be passed to the relevant [Seaborn](https://seaborn.pydata.org/api.html) function (see table below).
Can also use interpret the following arguments (not part of Seaborn, but added in PyBerries):
- xlabel, ylabel (str): X and Y axis labels
- xlim, ylim (tuple): plot limits for X and Y axis
- title (str): plot title

| Preset | Seaborn function that receives kwargs |
| ---- | ---- |
| histogram | [histplot](https://seaborn.pydata.org/generated/seaborn.histplot.html) |
| bar | [barplot](https://seaborn.pydata.org/generated/seaborn.barplot.html) |
| line | [lineplot](https://seaborn.pydata.org/generated/seaborn.lineplot.html) |
| line_fit | [lineplot](https://seaborn.pydata.org/generated/seaborn.lineplot.html) |
| histogram_fit | [histplot](https://seaborn.pydata.org/generated/seaborn.histplot.html) |
| scatter | [scatterplot](https://seaborn.pydata.org/generated/seaborn.scatterplot.html) |
| boxplot | [boxplot](https://seaborn.pydata.org/generated/seaborn.boxplot.html) |
| violinplot | [violinplot](https://seaborn.pydata.org/generated/seaborn.violinplot.html) |
| datapoints_and_mean | [stripplot](https://seaborn.pydata.org/generated/seaborn.stripplot.html) |
| heatmap | [histplot](https://seaborn.pydata.org/generated/seaborn.histplot.html) |
| timeseries | [scatterplot](https://seaborn.pydata.org/generated/seaborn.scatterplot.html) |
| boxenplot | [boxenplot](https://seaborn.pydata.org/generated/seaborn.boxenplot.html) |
| spot_tracks | [lineplot](https://seaborn.pydata.org/generated/seaborn.lineplot.html) |
| rates_summary | [pointplot](https://seaborn.pydata.org/generated/seaborn.pointplot.html) |
| grey_lines_and_highlight | [lineplot](https://seaborn.pydata.org/generated/seaborn.lineplot.html) |


## Examples
Histogram plot:
```python
plot_args = {'x':'SpineLength',
            'hue':'Group',
            'binwidth':2,
            'stat':'probability',
            'common_norm':False,
            'errorbars':None,
            'title':'',
            'xlabel':'Cell length (µm)',
            'ylabel':'Probability',
            'xlim':(None, None),
            'ylim':(None, None),
            'multiple':'layer',
            'element':'poly',
            'kde':False,
            'palette':'deep',
            }

data.plot_preset(preset='histogram', object_name='Bacteria', **plot_args)
```

Line plot with highlighted cell:
```python
cell_highlight = 0
highlight_by_index = True

plot_args = {'x':'Time',
            'y':'SpineLength',
            'title':'',
            'xlabel':'Time (min)',
            'ylabel':'Cell length (µm)',
            'xlim':(None, None),
            'ylim':(None, None),
            'color':'gray',
            'alpha':0.4,
            'estimator':None,
            'units':'Idx'
            }

ax = data.plot_preset(preset='grey_lines_and_highlight', object_name='Bacteria', highlight=cell_highlight, highlight_by_index=highlight_by_index, return_axes=True, **plot_args)
```

Plot for categorical vs. continuous data, showing individual data points (dots) and average per category (diamonds):
```python
plot_args = {'x':'SpineLength',
            'y':'SpotCount',
            'hue':'Group',
            'title':'',
            'xlabel':'Cell length (µm)',
            'ylabel':'Number of spots',
            'xlim':(None, None),
            'ylim':(None, None),
            'palette':'deep',
            'jitter':True,
            }

data.plot_preset(preset='datapoints_and_mean', object_name='Bacteria', **plot_args)
```