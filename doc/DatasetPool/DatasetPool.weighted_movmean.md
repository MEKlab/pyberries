# pyberries.data.DatasetPool.weighted_movmean

```python
DatasetPool.weighted_movmean(col, window, weights, object_name=None, inplace=False)
```

Calculates a weighted moving average of the specified column.

Note: make sure your DataFrame is ordered in the right way (e.g. along time) before performing this operation.

## Parameters

### **col**: *string*
Column name to compute the moving average on

### **window**: *integer*
Window (number of values) to compute the moving average on

### **weights**: *string*
Column to use as weights in the moving average

### **object_name**: *string or list of strings*
Object(s) to be processed

### **inplace**: *bool*
Set to True to modify the DatasetPool in place, to False (default) to return the modified DatasetPool

## Examples
```python
data = data.weighted_movmean(object_name='Bacteria', col='SpineLength', window=10, weights='nCells')
```
