# pyberries.data.DatasetPool.has_na

```python
DatasetPool.has_na(object_name=None)
```

Displays the number of NA values for each non-default column of each object.

## Parameters

### **object_name**: *string or list of strings*
Object(s) for which to print number of NA values.

## Examples
```python
data.has_na()
```

```python
data.has_na(object_name='Bacteria')
```
