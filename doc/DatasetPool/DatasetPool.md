# pyberries.data.DatasetPool

```python
class pyberries.data.DatasetPool(path, dsList, groups=[], metadata=[], filters={}, rename_cols={}, rename_objects={})
```

## Parameters

### **path**: *list of strings*
Path to the Bacmman folder that contains the datasets in dsList.
The list can contain a single path (same folder for all datasets) or as many paths as there are datasets in dsList.

### **dsList**: *list of strings*
Names of the Bacmman datasets to be imported.

### **groups**: *list of strings*
Legend entries (one per dataset in dsList).
Datasets that have the same group name will be averaged, and error bars can be shown.
If left empty, a different number will be assigned to each dataset (0, 1, 2...)

### **metadata**: *list of strings*
The metadata value for each position will be added to the specified object table.
Can contain a single string, or a list of strings to add all corresponding metadata columns.

### **filters**: *dict {object_name:filter}*
The specified object table will be filtered (using [pandas.DataFrame.query](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.query.html)).
*object_name* can be set to 'All' to apply the filter to all tables.
*filter* can be a single string, or a list of strings of equal length to dsList, to apply different filters to each dataset.

### **rename_cols**: *dict {old_name:new_name}*
Rename columns in all data tables *before* they are imported into the DatasetPool.

### **rename_objects**: *dict {old_name:new_name}*
Rename imported objects *before* they are imported into the DatasetPool.


## Attributes
| Attribute | Description |
| ---- | ---- |
| path | Bacmman path |
| dsList | List of dataset names |
| *object_name* | Measurement table for the corresponding object |
| parent | Dict of parent objects *{object_name:parent_object}* |
| channel | Dict of channel numbers *{object_name:channel_number}* |
| objects | List of Bacmman objects (dict keys of the table attribute) |
| positions | Dict of position names for each dataset *{dataset_name:list of positions}* |
| object_index | Dict of indices of the different objects (0, 1, 2...) |
| channelImage | Dict of the channel indices for each object (two objects detected on the same image will have the same channelImage) |


## Methods
| Method | Description |
| ---- | ---- |
| [add_from_child](DatasetPool.add_from_child.md)(object_name, child, col, multiple, rename, inplace) | Add a column with values taken from a child table |
| [add_from_parent](DatasetPool.add_from_parent.md)(object_name, col, inplace) | Add a column with values taken from a parent table |
| [add_metadata](DatasetPool.add_metadata.md)(keys, inplace) | Adds a column with a value taken from image metadata |
| [add_selection](DatasetPool.add_selection.md)(selection_name, inplace) | Add selection information exported from Bacmman to the corresponding measurement table |
| [apply_filters](DatasetPool.apply_filters.md)(filters, inplace) | Filters a table based on one or several columns |
| [assign](./DatasetPool.assign.md)(**kwargs) | Create or update a column in the target dataframes, using [pandas' assign](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.assign.html) |
| [bin_column](./DatasetPool.bin_column.md)(col, binsize, binlabels) | Create columns with binned values from the target columns |
| [classify](./DatasetPool.classify.md)(col, categories) | Create a column with custom values set by logical conditions |
| [copy](./DatasetPool.copy.md)() | Returns a copy of the DatasetPool |
| [describe](DatasetPool.describe.md)(agg, object_name, include) | Prints the chosen summary statistic for each numerical column of each dataset |
| [drop_duplicates](DatasetPool.drop_duplicates.md)(object_name, inplace, **kwargs) | Remove duplicated rows |
| [dropna](./DatasetPool.dropna.md)(object_name, inplace, **kwargs) | Removes NA values |
| [fillna](./DatasetPool.fillna.md)(object_name, col, inplace, **kwargs) | Fills NA values with a fixed value or an extrapolation |
| [filter_parent](DatasetPool.filter_parent.md)(source, inplace) | Filter out lines from a parent table if they are not contained in the specified child tables |
| [fuse_columns](DatasetPool.fuse_columns.md)(obj, columns, new, inplace) | Add a column with the contents of two or more columns, separated by a delimiter |
| [get_idx](DatasetPool.get_idx.md)(obj, idx, indices, newcol, inplace) | Add a column with one index extracted from an Indices column |
| [get_parent_indices](DatasetPool.get_parent_indices.md)(obj, indices, newcol, inplace) | Add a column with indices of the parent object |
| [has_na](./DatasetPool.has_na.md)(object_name) | Prints the number of NA values in each non-default column |
| [head](DatasetPool.head.md)(nlines) | Prints the first few lines of every data table |
| [heatmap_metrics](./DatasetPool.heatmap_metrics.md)() | Adds normalised spot positions along long and short axis and "centered" positions without normalisation |
| [load_datasets](DatasetPool.load_dataset.md)(ds_path, ds, grp, preprocessing, inplace) | Adds measurement tables from specified dataset to the DatasetPool |
| [pca](./DatasetPool.pca.md)(include, n_components, scaling) | Perform PCA on the specified columns |
| [plot_preset](DatasetPool.plot_preset.md)(preset, object_name, timeseries, drop_duplicates_by, return_axes, **kwargs) | Make pre-defined plot from the selected table |
| [propagate_filters](DatasetPool.propagate_filters.md)(parent, child, inplace) | Filter out lines from child table if their parent is not present in the parent table |
| [rename_object](DatasetPool.rename_objects.md)(rename, inplace) | Rename an object in the DatasetPool and merge tables if the new name is an already-existing object |
| [save_selection](DatasetPool.save_selection.md)(object_name, datasets, name) | Save a selection of objects to Bacmman |
| [set_type](./DatasetPool.set_type.md)(object_name, type_dict, inplace) | Change data type of specified columns |
| [split_column](./DatasetPool.split_column.md)(col, new_cols, delimiter, object_name, inplace) | Split the specified string column according to a delimiter |
| [tracking_Dapp](./DatasetPool.tracking_Dapp.md)(trim, exp_time) | Calculate Dapp values (requires the Bacmman measurement "MotionMetrics") |
| [weighted_movmean](./DatasetPool.weighted_movmean.md)(col, window, weights) | Calculates a weighted moving average on the specified column |
