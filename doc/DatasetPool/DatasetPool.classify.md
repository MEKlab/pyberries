# pyberries.data.DatasetPool.classify

```python
DatasetPool.classify(col, categories, object_name=None, inplace=False)
```

Adds a column to the specified object(s) with values set by logical conditions.

## Parameters

### **col**: *string*
Name of the new column to be created. Can also be an existing column, in which case it will be updated.

### **categories**: *dict*
Dictionnary with the format `{*condition*: *value*}`.

### **object_name**: *string or list of strings*
Object(s) to be processed

### **inplace**: *bool*
Set to True to modify the DatasetPool in place, to False (default) to return the modified DatasetPool

## Examples
```python
data = data.classify(object_name='Bacteria', col='cell_size', categories={'Length > 2.5': 'long', 'Length <= 2.5': 'short'})
```
