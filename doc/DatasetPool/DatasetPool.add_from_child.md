# pyberries.data.DatasetPool.add_from_child

```python
DatasetPool.add_from_parent(object_name, child, col=[], agg='mean', rename=[], inplace=False)
```

Add a column value from a child object to the specified parent object based on indices. In case of multiple children objects for the same parent, an aggregation (mean, max...) is performed.

## Parameters

### **object_name**: *string*
Parent object table to which the column will be added

### **child**: *string*
Child object table to take the data from

### **col**: *list of strings*
Name of the columns to be added to *object_name*

### **agg**: *string*
Aggregation method to be used in case of multiple child values for the same parent. For example: mean, median, max, min...

### **rename**: *list of strings*
Rename the column to be added to the parent table. The list must be of the same length than *col*.

### **inplace**: *bool*
Set to True to modify the DatasetPool in place, to False (default) to return the modified DatasetPool


## Examples
For each Bacteria, add the minimum distance between a CFP and an mCherry spots (as found in the CFP_spots table):
```python
data.add_from_child(object_name='Bacteria', child='CFP_spots', col='DistCC_oc2', agg='min', rename='CFP_mCherry_min_dist', inplace=True)
```