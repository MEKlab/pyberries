# pyberries.data.DatasetPool.tracking_Dapp

```python
DatasetPool.tracking_Dapp(trim=0, exp_time=1, object_name=None, inplace=False)
```

Calculate Dapp value for tracks with number of steps (i.e. localisations) >= to "trim". Requires the target table to contain "t0_IntervalCount_1" and "t4_MSD_1" columns (provided by the Bacmman measurement "MotionMetrics").

## Parameters

### **trim**: *integer*
Minimum number of steps in the track

### **exp_time**: *float*
Exposure time

### **object_name**: *string or list of strings*
Object(s) to be processed

### **inplace**: *bool*
Set to True to modify the DatasetPool in place, to False (default) to return the modified DatasetPool

## Examples
```python
data = data.tracking_Dapp(object_name='Spot_detection', trim=4, exp_time=0.012)
```
