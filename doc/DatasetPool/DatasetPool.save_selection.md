# pyberries.data.DatasetPool.save_selection

```python
DatasetPool.save_selection(object_name=None, datasets=None, name=None)
```

Save all objects contained corresponding to the specified *object_name* and *datasets* as a selection in Bacmman.

Note: Bacmman must be open for the saving to work.

## Parameters

### **object_name**: *string*
Object table to use (selection will contain all objects present in the table, based on the Position and Indices columns).

### **datasets**: *list*
List of names of datasets for which to save the selection. If set to None (default), selection will be saved for all datasets in the DatasetPool.

### **name**: *string*
Name of the selection to be created


## Examples
```python
data.apply_filters({'Bacteria':'SpineLength > 3.5'}).save_selection(object_name='Bacteria', name='Long_cells')
```