# pyberries.data.DatasetPool.dropna

```python
DatasetPool.dropna(object_name=None, inplace=False, **kwargs)
```

Removes NA values using pandas DataFrame.dropna method.

## Parameters

### **object_name**: *string or list of strings*
Object(s) to be processed

### **inplace**: *bool*
Set to True to modify the DatasetPool in place, to False (default) to return the modified DatasetPool

### ****kwargs**
Any keyword arguments to be passed to [Dataframe.dropna](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.dropna.html)

## Examples
```python
data.dropna(inplace=True)
```

```python
data.dropna(object_name='Bacteria', axis='columns', inplace=True)
```
