# pyberries.data.DatasetPool.drop_duplicates

```python
DatasetPool.drop_duplicates(object_name=None, inplace=False, **kwargs)
```

Remove duplicated rows using pandas DataFrame.drop_duplicates method.

Note: by default, the first of the duplicated rows is kept. Specify `keep='last'` to keep the last one instead.

## Parameters

### **object_name**: *string or list of strings*
Object(s) to be processed

### **inplace**: *bool*
Set to True to modify the DatasetPool in place, to False (default) to return the modified DatasetPool

### ****kwargs**
Any keyword arguments to be passed to [Dataframe.drop_duplicates](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.drop_duplicates.html)

## Examples
Remove rows that are identical across all columns:
```python
data.drop_duplicates(inplace=True)
```

Remove rows in `Bacteria` that have the same combination of `Dataset` and `Position` (i.e. keep only one row per Dataset and Position):
```python
data.drop_duplicates(object_name='Bacteria', subset=['Dataset', 'Position'], inplace=True)
```
