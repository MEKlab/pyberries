# pyberries.data.DatasetPool.apply_filters

```python
DatasetPool.apply_filters(fiters=None, inplace=False)
```

Filters DatasetPool tables based on column values.

Note: this function is called when creating a DatasetPool. It is recommended to specify filters to be applied in the DatasetPool creation rather than calling this function in your own scripts/notebooks.

## Parameters

### **filters**: *dict {object_name:filter}*
- *object_name (string)*: name of the object table to be filtered. Can be set to 'all' to filter all tables.
- *filter (string or list of strings)*: filters to be applied, will be passed to [pandas.DataFrame.query](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.query.html). If providing a single string, the filter will be applied to all Datasets in the DatasetPool. Alternatively, a list can be provided, with exactly one filter per dataset (use an empty string to apply no filtering).

### **inplace**: *bool*
Set to True to modify the DatasetPool in place, to False (default) to return the modified DatasetPool


## Examples
Simple threshold (keep only cells longer than 2.5 µm):
```python
data.apply_filters({'Bacteria':'SpineLength > 2.5'}, inplace=True)
```

Combine two conditions (also works with the "or" keyword):
```python
data.apply_filters({'Bacteria':'SpineLength > 2.5 and CFPCount >= 1'}, inplace=True)
```

Set a condition by comparing the values of two different columns:
```python
data.apply_filters({'Bacteria':'CFPCount > mCherryCount'}, inplace=True)
```

Apply a filter to one Dataset but not the other (here "data" contains 2 Datasets):
```python
data.apply_filters({'Bacteria':['Time > 60', '']}, inplace=True)
```