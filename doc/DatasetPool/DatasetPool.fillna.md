# pyberries.data.DatasetPool.fillna

```python
DatasetPool.fillna(object_name=None, inplace=False, **kwargs)
```

Replace NA values using pandas DataFrame.fillna method.

## Parameters

### **object_name**: *string or list of strings*
Object(s) to be processed

### **col**: *string or list of strings*
Columns in target tables to be processed

### **inplace**: *bool*
Set to True to modify the DatasetPool in place, to False (default) to return the modified DatasetPool

### ****kwargs**
Any keyword arguments to be passed to [Dataframe.fillna](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.fillna.html#pandas-dataframe-fillna)

## Examples
Replace all missing values with 0 (in all tables):
```python
data.fillna(0, inplace=True)
```

Replace missing values with 0 in the `SpineLength` and `SpineWidth` columns of `Bacteria`:
```python
data.fillna(value=0, object_name='Bacteria', col=['SpineLength', 'SpineWidth'], inplace=True)
```

Replace missing values in `Bacteria` with the previous value in the column:
```python
data.fillna(object_name='Bacteria', method='ffill', inplace=True)
```
