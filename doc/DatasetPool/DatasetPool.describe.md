# pyberries.data.DatasetPool.describe

```python
DatasetPool.describe(agg, object_name=None, include='all', by='Dataset')
```

Generate descriptive statisctics for each Dataset contained in DatasetPool.

## Parameters

### **agg**: *string or list of strings*
Function(s) to use for aggregating the data.

Examples of functions that can be used:
- mean: Mean
- sum: Sum
- std: Standard deviation
- var: Variance
- sem: Standard error of the mean
- first: First value
- last: Last value
- min: Minimum value
- max: Maximum value

### **object_name**: *string or list of strings*
Object table to be displayed. Displays all tables if none specified (*default*).

### **include**: *string or list of strings*
Column(s) to perform the calculations on (must be numerical). Defaults to 'all' (apply aggregation function to all numerical columns).

### **by**: *string or list of strings*
Column(s) used to define groups for the aggregation. Defaults to `'Dataset'`, i.e. the output table will compute summary statistics for each dataset separately.
