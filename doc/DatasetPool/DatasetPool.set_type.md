# pyberries.data.DatasetPool.set_type

```python
DatasetPool.set_type(object_name=None, type_dict, inplace=False)
```

Set column data types.

## Parameters

### **object_name**: *string or list of strings*
Object(s) to be processed

### **type_dict**: *dict*
Dictionnary with the format `{*Column_name*: *data_type*}`.

### **inplace**: *bool*
Set to True to modify the DatasetPool in place, to False (default) to return the modified DatasetPool

## Examples
Change the type of `Group` and `SpotCount` columns to categorical:
```python
data.set_type(type_dict={'Group':'category', 'SpotCount':'category'}, inplace=True)
```

Change the type of `SpineLength` to float32, only in the `Bacteria` table:
```python
data.set_type(object_name='Bacteria', type_dict={'SpineLength':'float32'}, inplace=True)
```