# pyberries.data.DatasetPool.add_metadata

```python
DatasetPool.add_metadata(metadata=None, inplace=False)
```

Add columns to DatasetPool tables with values read from metadata files.

Notes:

This requires your dataset folder to contain a "SourceImageMetadata" folder (should be created automatically when importing images to a Bacmman dataset).

This function is called when creating a DatasetPool. It is recommended to specify metadata to be added in the DatasetPool creation rather than calling this function in your own scripts/notebooks.

## Parameters

### **keys**: *list*
- *keys*: name of the metadata field to be read from the files found in the SourceImageMetadata folder. A list of strings can be provided to add several metadata columns to the table. See below for a list of common metadata fields when acquiring images with Metamorph.

### **inplace**: *bool*
Set to True to modify the DatasetPool in place, to False (default) to return the modified DatasetPool

## Examples
```python
data.add_metadata('DateTime', inplace=True)
```
DateTime corresponds to the exact time at which the image was taken.

```python
data.add_metadata(['DateTime', 'ASI X', 'ASI Y'], inplace=True)
```
'ASI X' and 'ASI Y' correspond to the X and Y stage positions at which the image was taken.

## Common metadata fields with example values (image recorded with Metamorph software)
- ASI X=20617.2
- ASI Y=-5459.15
- DateTime=20220428 16:54:37.574
- Exposure=30 ms
- Frames to Average=1
- ImageLength=512
- ImageWidth=512
- Multiplication Gain=4
- Ti Filter Block 1=TRITC
- Ti Objective=Plan Apo TIRF 100x / 1.45
- Ti Z=3863.2
- image-name=0 Brightfield
- number-of-planes=16
- scale-max=6856
- scale-min=4989
- spatial-calibration-units=um
- spatial-calibration-x=0.106915
- spatial-calibration-y=0.106915
- stage-position-x=20617.2
- stage-position-y=-5459.15
- z-position=3863.2