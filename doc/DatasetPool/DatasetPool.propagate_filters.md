# pyberries.data.DatasetPool.propagate_filters

```python
DatasetPool.propagate_filters(parent, child, inplace=False)
```

Remove all objects from the target table whose parent object have been filtered out.

Note: this function is applied systematically by `apply_filters` and when filtering during DatasetPool creation. You do not need to call it if you used any of these two methods to filter your data.

## Parameters

### **parent**: *string*
Name of the parent object to use as a reference

### **child**: *string*
Name of the child object to filter based on parent object

### **inplace**: *bool*
Set to True to modify the DatasetPool in place, to False (default) to return the modified DatasetPool


## Examples
```python
data.propagate_filters(parent='Bacteria', child='Spot_detection', inplace=True)
```