# pyberries.data.DatasetPool.split_column

```python
DatasetPool.split_column(col, new_cols, delimiter, object_name=None, inplace=False)
```

Splits the specified column into new columns according to the delimiter.

Notes:
- This will only work on columns of type "string"
- This will not work for variable numbers of delimiters: each line must contain exactly the same number of delimiters

## Parameters

### **col**: *string*
Name of the column to be split

### **new_cols**: *list of strings*
Names of the newly created columns. Must contain exactly one name per expected new column

### **delimiter**: *string*
Delimiter according to which to split the column

### **object_name**: *string*
Object table to be processed

### **inplace**: *bool*
Set to True to modify the DatasetPool in place, to False (default) to return the modified DatasetPool


## Examples
```python
data.split_column(object_name='Bacteria', col='Indices', new_cols=['Indices_0', 'Indices_1'], delimiter='-', inplace=True)
```