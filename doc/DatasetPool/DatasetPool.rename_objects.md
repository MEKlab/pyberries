# pyberries.data.DatasetPool.rename_objects

```python
DatasetPool.rename_objects(rename=None, inplace=False)
```

Renames objects contained in the DatasetPool and merges tables that have the same name.

This is mostly useful to merge tables that were given different names in Bacmman.

## Parameters

### **rename**: *dict {old_name:new_name}*
- *old_name (string)*: current object name (read from the Bacmman config file during DatasetPool creation)
- *new_name (string)*: name to rename the object with

### **inplace**: *bool*
Set to True to modify the DatasetPool in place, to False (default) to return the modified DatasetPool

## Examples
```python
data.rename_objects({'Bacteria_large':'Bacteria'}, inplace=True)
```