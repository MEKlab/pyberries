# PyBerries main functionalities

## Installation

To install the package, use the following command in a terminal:

`python -m pip install PyBerries`

You can also install a specific version number (useful e.g. to make sure you code won't be broken by a future update):

`python -m pip install PyBerries==0.2.8`

In a jupyter notebook, use the command:

`%pip install PyBerries`, or `%pip install PyBerries==0.2.8` for a specific version.


## Creating a DatasetPool

📖 [DatasetPool documentation](./DatasetPool/DatasetPool.md)

To import Bacmman measurement tables with PyBerries, you must create a "DatasetPool" (an object that will contain one or several Bacmman datasets). The minimum required arguments to create a DatasetPool are:
- `dsList`: name(s) of the Bacmman datasets to be imported
- `path`: path to the Bacmman folder containing the datasets
  - Note: if datasets are stored in different folders, path can be given as a list with one path per dataset in dsList

Optional arguments can be added:
- `groups`: set legend labels for the datasets. If two datasets have the same label, they will be concatenated (and error bars can be shown if supported)
  - Format: `groups = ['Group1', 'Group2', 'Group3']` with a number of groups equal to the number of datasets in dsList
  - Example: `groups = ['WT', 'WT', 'Mutant']` will attribute the group "WT" to datasets 1 and 2, and the group "Mutant" to dataset 3
- `filters`: filter the datasets using the syntax of [pandas.DataFrame.query](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.query.html)
  - Format: `filters = {'object':'filter'}`, where object is the name of the target Bacmman object
  - Example: `filters = {'Bacteria':'SpineLength > 3'}` to keep only Bacteria that have a length > 3
  - Note that filtering an object will also filter out any child objects (e.g. if a bacteria is removed, the spots it contains will be removed as well)
- `metadata`: enter the name of a metadata field (found in the `SourceImageMetadata` folder of the dataset) to add a column with the corresponding metadata value for each position.
  - Format: `metadata = 'metadata_name'` for a single value, or `metadata = ['metadata1', 'metadata2']` for multiple values
  - Example: `metadata = 'DateTime'` will add the acquisition time for each position (in all tables)
- `rename_cols`: columns to be renamed (in all tables) upon import. This can be useful if a measurement has different names in different datasets
  - Format: `rename_cols = {'old_name':'new_name'}`
  - Example: `rename_cols = {'ObjectCount':'SpotCount'}` to rename all columns "ObjectCount" to "SpotCount"
- `rename_objects`: Bacmman objects to be renames upon import. This can be useful if two objects have different names in different datasets
  - Format: `rename_objects = {'old_name':'new_name'}`
  - Example: `rename_objects = {'Bacteria_large':'Bacteria'}` to rename all objects called "Bacteria_large" to "Bacteria"

Example of DatasetPool creation:
```python
from pyberries.data import DatasetPool
data = DatasetPool(path='D:/Daniel/BACMMAN/Timelapse', dsList=['230118_DT23'], metadata='DateTime')
```

### About filtering

Filtering is applied when creating a DatasetPool, but can also be applied afterwards with the `apply_filters` method. Example:
```python
data.apply_filters({'Bacteria':'SpineLength > 3'}, inplace=True)
```


## Data format

The Bacmman measurement tables will be imported, and tables from objects that have the same name will be concatenated as a single Pandas DataFrame. The `Dataset` column specifies which Bacmman dataset a given line belongs to.

Measurement tables are stored in a property with the object's name (e.g. `data.Bacteria` for the Bacteria table).

To display the data contained in the 'Bacteria' table, run in a Jupyter Notebook:
```python
display(data.Bacteria)
```

Note: the data.Bacteria table can also be accessed with the syntax `data['Bacteria']`.


## Dataset summary

You can use the `describe` method to print a summary of all numerical columns in the DatasetPool. One or several aggregation methods can be specified, for example:
```python
data.describe('median')
```
to print the median value for each column, or
```python
data.describe(['mean', 'std'])
```
to print mean and standard deviation.

Other aggregations are possible, including (but not limited to): `'max'`, `'min'`, `'sum'`, `'sem'`. For more details on aggregations, consult [pandas.DataFrame.aggregate](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.aggregate.html).

Output can be limited to certain columns by using the keyword `include`:
```python
data.describe(['mean', 'std'], include=['SpineLength', 'SpineWidth'])
```

You can also use the `head` method to display the first n lines of each table (similar to pandas `DataFrame.head()`):
```python
data.head(5)
```


## Adding columns

Several DatasetPool methods perform pre-set computations and add a column to the table with the result:
- [DatasetPool.heatmap_metrics](./DatasetPool/DatasetPool.heatmap_metrics.md)
- [DatasetPool.bin_column](./DatasetPool/DatasetPool.bin_column.md)
- [DatasetPool.tracking_Dapp](./DatasetPool/DatasetPool.tracking_Dapp.md)

Example use:
```python
data.bin_columnn(object_name='Bacteria', col='Time', binsize=15, inplace=True)
```

Note: as with all DatasetPool methods, omitting the `object_name` argument will perform the operation on all tables.


## Adding a column from a parent table

If 'Bacteria' is a parent of 'Spots', it is possible to add data from the parent table to the child's. For example if the 'Bacteria' table contains lineage information, we can add to each spot the lineage of its parent bacteria.

For example:
```python
data.add_from_parent(object_name='Spots', col='lineage', inplace=True)
```

Note that the parent table will be automatically inferred from the Bacmman configuration file.

## Adding a column from a child table

The reverse operation to `add_from_parent` is also possible. For example, we can add nucleoid size to its parent Bacteria. Since each parent can have several children, an aggregation method must be defined to collapse the multiple values (e.g. mean, sum...). Moreover, since a parent table can have several children, it is necessary to explicitly mention which child table the data should be taken from.

For example:
```python
data.add_from_child(object_name='Bacteria', child='Nucleoid', col='Size', agg='sum', inplace=True)
```


## Making figures

PyBerries uses Seaborn and Matplotlib to plot data. There are three different ways to create plots:
- Through a DatasetPool method (`plot_preset`)
  - Includes several plots which combine several elements (e.g. `plot_timeseries` which displays both a scatter and a lineplot)
- By directly using Seaborn plotting functions
  - You might want to have a look at [Seaborn's object interface](https://seaborn.pydata.org/tutorial/objects_interface), which provides an efficient and powerful way to manipulate and plot data
- Alternatively, any plotting library that can take a pandas DataFrame as input can be used


### DatasetPool preset plots

The plot preset function takes the following arguments:
- preset (str): type of plot to make
- object_name (str): table to plot from
- timeseries (bool): set to *True* to plot from a timeseries table, and to *False* (default) to plot from the normal measurement table
- drop_duplicates_by (list of str): before plotting, remove all lines that are duplicates according to the column (or combination of columns) specified
- return_axes (bool): return figure axis to enable further changes/additional plots to be added
- title (str): plot title
- xlabel, ylabel (str): X and Y axis labels
- xlim, ylim (2-tuple): X and Y axis limits
- **kwargs: any arguments to be passed to the seaborn plot

Available presets are:
- `histogram`
- `bar`
- `line`
- `scatter`
- `datapoints_and_mean`
- `heatmap`
- `timeseries`
- `boxplot`
- `boxenplot`
- `spot_tracks`
- `histogram_fit`
- `line_fit`
- `rates_summary`
- `grey_lines_and_highlight`

The additional argument `return_axes` can be passed to all dataset plot methods to enable further modifications to the figure:
```python
import seaborn as sns

ax = data.plot_preset(preset='histogram', object_name='Bacteria', return_axes=True, **plot_args)
sns.move_legend(ax, "upper left", bbox_to_anchor=(1, 1), labelspacing=1)
```
moves the legend outside of the plot.

### Using Seaborn's object interface
From version 0.12, Seaborn introduced a new interface to produce plots. Using this new "objects interface", the same histogram can be produced with:
```python
import seaborn.objects as so

(
    so.Plot(data.Bacteria, x='Bacteria_Size', color='Group')
    .add(so.Area(), so.Hist(binwidth=2, stat='probability', common_norm=False))
    .scale(color='deep')
    .limit(x=(None, None), y=(None, None))
    .label(x="Cell area (µm$^2$)", y="Probability", title='Plot title', color='Legend title')
)
```
Note that `so.Area()` can be replaced by `so.Bars()` to show histogram bars.
