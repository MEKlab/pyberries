# pyberries.stats.tukey_test

```python
tukey_test(data, col, by)
```

Use Tukey's test to compare average values for different groups against each other.

## Parameters

### **data**: *pandas DataFrame*
Table containing the data to be tested (e.g. data.Bacteria).

### **col**: *string*
Name of the column that contains data to be tested.

### **by**: *string*
Name of the column to use to group the data.


## Examples
Test if cell length is significantly different for cells with 0, 1, 2 or 3 spots, for each group independently:
```python
from pyberries.stats import tukey_test
res = tukey_test(data=data.Bacteria_large.query('SpotCount < 4'), col='SpineLength', by=['SpotCount', 'Group'])
```
