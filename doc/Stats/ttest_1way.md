# pyberries.stats.ttest_1way

```python
ttest_1way(data, col, test_mean)
```

Perform a 1-way t-test to determine if the average value of a column is significantly different from a value.

## Parameters

### **data**: *pandas DataFrame*
Table containing the data to be tested (e.g. data.Bacteria).

### **col**: *string*
Name of the column that contains data to be tested.

### **test_mean**: *float*
Value to test the mean of *col* against.


## Examples
Test if average cell length for Dataset1 is significantly different from 2 um.
```python
from pyberries.stats import ttest_1way
res = ttest_1way(data.Bacteria_large.query('Dataset == "Dataset1"'), col='SpineLength', test_mean=2)
```
