# pyberries.stats.ks_test

```python
ks_test(data, col, by)
```

Use the Kolomogorov-Smirnoff test to assess if a variable has a significantly different distribution between two groups.

## Parameters

### **data**: *pandas DataFrame*
Table containing the data to be tested (e.g. data.Bacteria).

### **col**: *string*
Name of the column that contains data to be tested.

### **by**: *string*
Name of the column to use to group the data. Must result in exactly two groups.


## Examples
Test if the cell length distribution of two different groups is significantly different:
```python
from pyberries.stats import ks_test
res = ks_test(data=data.Bacteria, col='SpineLength', by='Group')
```
