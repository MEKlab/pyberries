# pyberries.stats.ttest_2way

```python
ttest_2way(data, col, by, levene_threshold)
```

Perform a 2-way t-test on the specified column of a dataframe to assess whether their means are equal.

Before performing the test, samples are tested for equal variance using Levene's test. If variances are equal, perform a standard independent 2 sample test that assumes equal population variances. If variances are significantly different, perform Welch’s t-test, which does not assume equal population variance.

## Parameters

### **data**: *pandas DataFrame*
Table containing the data to be tested (e.g. data.Bacteria).

### **col**: *string*
Name of the column that contains data to be tested.

### **by**: *string*
Name of the column to use to group the data. Must result in exactly two groups. For more than 2 groups, use [Tukey's test](./tukey_test.md) instead.

### **levene_threshold**: *float*
Significance threshold for the Levene test for equal variance (default 0.05).


## Examples
To compare cell lengths between two groups:
```python
from pyberries.stats import ttest_2way
res = ttest_2way(data.Bacteria, col='SpineLength', by='Group')
```

If the DatasetPool contains more than 2 groups, groups can be selected using pandas query function:
```python
from pyberries.stats import ttest_2way
res = ttest_2way(data.Bacteria.query('Group == "Group1" or Group == "Group2"'), col='SpineLength', by='Group')
```
