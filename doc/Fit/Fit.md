# pyberries.data.Fit

```python
class pyberries.data.Fit(df_in, x, y, model=None, model_type=None, groupby='Group', p0=None, param_format='long', bootstrap_samples=1, verbose=False)
```

## Parameters

### **df_in**: *pandas DataFrame*
DataFrame that contains the data to be fitted

### **x**: *string*
Name of the column that contains X values for the fit

### **y**: *string*
Name of the column that contains Y values for the fit

### **model**: *function*
Function to use for fitting (note: leave this parameter empty if using *model_type*).

Example:
```python
def model(x, a, b, c):
    return a*np.exp(-b*x) + c
```

### **model_type**: *string*
Pre-defined model function to use for fitting. Currently available models are:
| Model name | Formula |
| ---- | ---- |
| monoexp_decay | y = Amplitude\*exp(-Rate\*x) |
| monoexp_decay_offset | y = Amplitude\*exp(-Rate\*x) + Offset |
| biexp_decay | y = Amplitude_1\*exp(-Rate_1\*x) + Amplitude_2\*exp(-Rate_2\*x) |
| linear | y = Slope\*x + Offset |

### **groupby**: *string*
Column to group the data by before fitting. An independent fit will be made for each group.

### **p0**: *list*
Initial values for the fit parameters. If given, should have a length equal to the number of fitted parameters.

### **param_format**: *string*
Format of the parameters dataframe, either `'long'` (default) or `'wide'`.

### **bootstrap_samples**: *integer*
If specified, perform each fit n times on samples of the same size as the original data, but with values drawn with replacement (so some values might be duplicated). This generates slight variation in the different fits. The returned fitted parameters will be the median value from all bootstraps.

### **verbose**: *boolean*
Set to `True` to display information about failed fits.


Note: you can use the `get_histogram` function to produce a histogram, and pass its result into *df_in* to fit it.

## Attributes
| Attribute | Description |
| ---- | ---- |
| model_type | Preset model used for fitting |
| model | model function used for fitting |
| data | DataFrame containing the raw and fitted data |
| parameters | Dataframe containing fit parameters for each group |
