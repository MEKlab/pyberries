from .test_statistics import *  # noqa: F401,F403
from .information_criteria import *  # noqa: F401,F403

name = 'stats'
