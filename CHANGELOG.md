# 0.2.26 (18/12/2024)

**Feature**
- Added `DatasetPool.classify` to create columns with categories set by conditional statements

# 0.2.25 (02/12/2024)

**Enhancements**
- `DatasetPool.describe()` now takes a `by` argument, to aggregate using different columns (e.g. `by='Position'`, or `by=['Dataset', 'Position']`)

**Fixes**
- `DatasetPool.apply_filters()` does not incorreclty clear tables when a parent exists but its table is empty (because not measurements have been made)

# 0.2.24 (27/11/2024)

**Enhancements**
- Allow data to be loaded if some of the measurement tables are missing (loader displays which tables are missing)

# 0.2.23 (27/11/2024)

**Fixes**
- Fixed metadata import sometimes failing when there are duplicated channels
- Fixed `DatasetPool.describe()` returning an error when multiple aggregations were provided

# 0.2.22 (02/10/2024)

**Fixes**
- Fixed `add_from_child` and changed `multiple` option name to `agg`
- Fixed pandas SettingWithCopy warning on `get_histogram`

**Enhancements**
- Added a `y_err` parameter to `Fit` to take standard deviation on the data points in account when fitting
- Added a `bootstrap_samples` parameter to `Fit` to perform bootstrapping on the fits. For example if bootstrapping is set to 10, each fit will be performed 10 times, and the fitted parameter will be the median over the 10 iterations.

# 0.2.21 (19/04/2024)

**Enhancements**
- Changed metadata import to DatasetPool to match the new .json format from Bacmman
    - The `metadata` argument to DatasetPool now must take a list of metadata fields to be added. The corresponding metadata will be added to all tables where it is relevant.
- Added `linear` as a predefined `model_type` in `pyberries.data.Fit`
- Made `Fit` create a user-friendly dataframe of fitted parameters, accessible under `Fit.parameters`
- `Fit` takes an argument `param_format` that can be set to `long` (default) or `wide` to change the format of the output `parameters` dataframe

**Deprecations**
- Metadata in .txt format will no longer be read. Only the new .json format is accepted.
    - For old datasets, re-import your data to create the json files (the txt files can be deleted)
- Removed `Fit.fit_results` (replace by `Fit.parameters`)
- Removed `pyberries.data.Fit.get_rates`
- Removed `pyberries.data.Fit.get_fit_parameters`

# 0.2.20 (19/04/2024)

Skip this version (wrong upload).

# 0.2.19 (29/02/2024)

**Fixes**
- Fixed `filter_parent` so that when the parent is filtered, all child objects are removed too

# 0.2.18 (20/09/2023)

**Fixes**
- Allow reading the new Bacmman config format (introduced on 15/09/2023). This update is necessary for all datasets created after this date

**Enhancements**
- Added fit residuals to Fit.data

# 0.2.17 (08/09/2023)

**Fixes**
- Fixed a bug in bin_column that was causing wrong histograms to be computed from get_histogram

# 0.2.16 (04/09/2023)

**Features**
- Added an `assign` method to DatasetPool that applies `pandas.DataFrame.assign` to the specified object tables
- Added `aic` and `bic` properties to the Fit class, that contains respectively the Akaike and Bayesian Information Criteria (AIC/BIC) for each fit

**Enhancements**
- Changed the column names of the `heatmap` metric of `add_columns` to make them more explicit
    - normXpos -> `normLongCoord` (spot position on the long axis, centered and normalised by cell length)
    - normYpos -> `normShortCoord` (spot position on the long axis, centered and normalised by cell width)
    - x_fork -> `centerLongCoord` (spot position on the long axis, centered but not normalised)
    - y_fork -> removed, since it was only a copy of the SpineLength column

Note: the `SpineRadialCoord` column from Bacmman is already centered, so we don't need to create a "centerShortCoord" column.

- Added individual methods for metrics (previously available through `add_columns`)
    - `bin_column`
    - `heatmap_metrics`
    - `tracking_Dapp`
    - `pca`
    - `weighted_movmean`

**Deprecations**
- Removed the `add_columns` method. Use the individual methods listed above instead (`bin_column`, `heatmap_metrics`,...)
- Removed the `is_col_larger` metric. Use the new `assign` method instead
- Removed the `normalise` method. Use the new `assign` method instead
- Removed the `get_timeseries` method. Instead, use `bin_column` to do time binning, and `assign` or `get_histogram` to compute metrics of interest

# 0.2.15 (26/07/2023)

**Features**
- Added `violinplot` to plot presets
- Added a `discrete` option (boolean) to `pyberries.data.get_histogram` to make histograms of categorical data

**Fixes**
- Fixed `add_from_child` incorrectly filtering out objects from the recipient table when they had no children
- Fixed `add_from_child` and `add_from_parent` not working correctly with multiple datasets

# 0.2.14 (10/07/2023)

**Features**
- Added `add_from_child` method to DatasetPool to add a column from a child table to a parent. An argument allows to choose how to deal with multiple children for the same parent (average, min, max...)
- Added `add_selection` method to DatasetPool to import selection information exported from Bacmman to the corresponding measurement table

# 0.2.13 (02/06/2023)

**Enhancements**
- Added possibility to subtract an offset to fitted rates in `Fit.get_rates`

**Features**
- Added a `load_dataset` method to DatasetPool to add a new dataset to an existing DatasetPool
- Added a function `Add_significance` to display significance bars on Seaborn object plots
- Added a `pyberries.stats` module, with functions to perform statistical tests
    - See table below for available tests
    - See the `Stats_tutorial` in the Tutorial folder for example use

**Statistical tests**
`data` refers to a pandas DataFrame, e.g. `data.Bacteria`
| Test | Function | Usage |
| ---- | ---- | ---- |
| One-sided t-test | ttest_1way(data, col, test_mean) | Tests if the average of "col" is equal to the test_mean value |
| Two-sided t-test | ttest_2way(data, col, by, levene_threshold) | Tests if the average of col is different between 2 groups (defined by the "by" keyword) |
| Tukey's test | tukey_test(data, col, by) | Pairwise test for difference in means between multiple groups (defined by the "by" keyword) |
| Kolmogorov-Smirnoff test | ks_test(data, col, by) | Tests if two continuous distributions are different (defined by the "by" keyword) |

# 0.2.12 (26/05/2023)

**Features**
- Added a metric for PCA computation (works only on numeric columns). Example use: `data.add_columns(object_name='Bacteria', metrics='PCA', include=['SpineLength', 'SpineWidth', 'SpotCount'], inplace=True)`
- Added a `filter_parent` method to DatasetPool to filter out parent objects that are not contained in a child table (e.g. remove Bacteria where spots don't meet a condition)

**Deprecations**
- Removed the `drop_duplicates_by` argument of plot_preset. Use method chaining to achieve the same purpose, e.g. `data.drop_duplicates(subset=['Dataset', 'Position']).plot_preset(...)`

# 0.2.11 (24/05/2023)

**Feature**
- Added save_selection method to DatasetPool to save a selection of objects to Bacmman.

**Fixes**
- Fixed adding metadata and filtering during DatasetPool creation, which were broken in 0.2.10

# 0.2.10 (23/05/2023)

**Features**
- Added a `DatasetPool.set_type` method which allows setting the data types of columns by providing a dictionary: {*column_name*:*dtype*}. Examples of dtypes: 'int32', 'float64', 'category'
- Added a `split_column` method to split a string column according to a delimiter
- Added a `copy` method to DatasetPool (e.g. `data_copy = data.copy()`)
- Added a `Fit` class to fit custom functions on data

**Enhancements**
- Added `drop_duplicates` method to DatasetPool (see [pandas drop_duplicates](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.drop_duplicates.html) for options)
- Added an `inplace` keyword to all data-modifying methods of DatasetPool
    - True: same behaviour as previously (updates the DatasetPool object)
    - False (*default*): returns a new DatasetPool object
    - Note that this allows operation chaining, e.g. `data = data.add_metadata(metadata).apply_filters(filters)`
- Allow specifying `object_name` in `DatasetPool.describe` to show only specified tables

**Fixes**
- Fixed add_column (broken in 0.2.9).

# 0.2.9 (17/05/2023)

**Enhancements**
- Data tables are now accessed directly through a DatasetPool property. Example: instead of `data.table['Bacteria_large']`, write `data.Bacteria_large` or `data['Bacteria_large']`
- Timeseries tables are stored in a new object called *object_name*_timeseries. Example: after running DatasetPool.get_timeseries on `Bacteria_large`, the corresponding table will be stored under data.Bacteria_large_timeseries.
- Added pointplot to available Seaborn plots (use with `pyberries.plots.pointplot`)

**Features**
- Added pandas methods to DatasetPool, which can be applied to all objects, or specific ones:
    - `DatasetPool.has_na()`: prints the number of NA values in each non-default column
    - `DatasetPool.dropna()`: removes NA values in tables (see [pandas DataFrame.dropna](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.dropna.html) for options)
    - `DatasetPool.fillna()`: fills NA values with a fixed value or an extrapolation (see [pandas DataFrame.fillna](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.fillna.html#pandas-dataframe-fillna) for options)

**Fixes**
- Update parent names when changing object name with DatasetPool.rename_object()

# 0.2.8.post1 (10/05/2023)

**Fixes**
- Fixed `DatasetPool.describe` crash when using *include* on a column that is not present in all tables

**Features**
- Added new DatasetPool method `head` to display the first few lines of all data tables. Example use: `data.head(5)`

# 0.2.8 (10/05/2023)

**Enhancements**
- When using plot_preset, *hue* can be given a list of column names to plot one group per unique combination of these column names

**Features**
- Fit ensemble fluorescence decays from SNR measurement instead of reading h5 file
- Added Seaborn boxplot. Use with the `boxplot` preset, or `from pyberries.plots import boxplot`.
- Added new preset `grey_lines_and_highlight` which plots multiple observations (defined by the `units` parameter) as faint grey lines, and highlights one specified line in red.

**Fixes**
- Fixed metadata import for timelapse data

**Deprecations**
- DatasetPool.add_background is removed; the preferred workflow is to use Bacmman measurements to either:
    - Calculate an object's SNR (`intensity - background` or `intensity - std(background)`)
    - Calculate the average background signal per frame (using ExcludeRegions)

# 0.2.7 (05/05/2023)

**Fixes**
- Fixed parent tables being set incorrectly when importing a dataset with no 'Viewfield' table (index -1).

# 0.2.6.post1 (01/05/2023)

This is the first working version for pip install.
