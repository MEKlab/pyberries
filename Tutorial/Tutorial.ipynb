{
 "cells": [
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Tutorial notebook\n",
    "\n",
    "Welcome to this tutorial notebook for the PyBerries package.\n",
    "\n",
    "This notebook will illustrate how to explore Bacmman measurement tables using PyBerries.\n",
    "\n",
    "First, let's install PyBerries:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%pip install PyBerries==0.2.25"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Specifying the version number ensures that your workflow will not be broken in a future release, and that someone else using the same package version will get consistent results.\n",
    "\n",
    "You can find all available versions on [PyPi](https://pypi.org/project/PyBerries/), and details on each of them is available in the [changelog](https://gitlab.com/MEKlab/pyberries/-/blob/main/CHANGELOG.md)\n",
    "\n",
    "## Creating a DatasetPool\n",
    "\n",
    "Download and unzip the [example datasets](https://gitlab.com/MEKlab/pyberries/-/raw/main/Tutorial/Example_datasets.zip?inline=false).\n",
    "\n",
    "For this tutorial, your \"Bacmman folder\" will be the \"Example_datasets\" folder you have just downloaded.\n",
    "\n",
    "In this folder, you can see four datasets called \"Dataset1\" to \"Dataset4\".\n",
    "\n",
    "We will now create a \"DatasetPool\", which contains all measurement tables for our four datasets.\n",
    "\n",
    "Note:\n",
    "- Change `path` to match the path of the \"Example_datasets\" folder you have unzipped\n",
    "- On Windows, replace `\\` by `\\\\` in the path"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "path = 'C:\\\\Users\\\\dthedie\\\\Downloads\\\\Example_datasets'\n",
    "dsList = ['Dataset1', 'Dataset2', 'Dataset3', 'Dataset4']\n",
    "\n",
    "from pyberries.data import DatasetPool\n",
    "data = DatasetPool(path=path, dsList=dsList)"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The output shows you which Bacmman objects are available in each dataset.\n",
    "\n",
    "Here we have:\n",
    "- `Bacteria_large`: the segmented cells\n",
    "- `Spot_detection`: fluorescent spots contained in the cells\n",
    "\n",
    "## Exploring the data\n",
    "\n",
    "Before we start making graphs, let's see what our datasets contain.\n",
    "\n",
    "Using `describe('mean')` method shows the average value for all numerical columns, as well as the number of objects in the table (ignore the \"Group\" column for now):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "data.describe('mean')"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note that the column `SpineLength` is present in both tables, with slightly different mean values. This is because the \"Spot_detection\" table has one line per segmented spot, and therefore contains no information about cells with no spots.\n",
    "\n",
    "To get an idea of the general cell size distribution in our dataset, we should therefore use the \"Bacteria_large\" table (examples will come below).\n",
    "\n",
    "To get more information on a specific column, we can provide multiple metrics as a list, and the keyword `include` (can also be a list to include several columns):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "data.describe(['mean', 'std', 'min', 'median', 'max'], include='SpineLength')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To group data by other columns that just the dataset, we can use the `by` keyword.\n",
    "\n",
    "For example, here we group the results both by dataset and position:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "data.describe(by=['Dataset', 'Position'], include='SpineLength')"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can also use `head` (similar to the pandas function) to display the first few lines of each data table:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "data.head(5)"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Finally, we can check what are the parents of each of our objects:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "display(data._parents)"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\"Bacteria_large\" does not have a parent object (None), while the parent of \"Spot_detection\" is \"Bacteria_large\" (because spots are contained in Bacteria)."
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Filtering data\n",
    "\n",
    "When creating a DatasetPool, use the `filters` argument to remove rows based on a specific column.\n",
    "\n",
    "`filters` takes a dictionary of *table:query* pairs, where *table* is the measurement table to be filtered, and *query* is the filter to be applied to that table (in the format of [panda's query method](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.query.html))."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "filters = {'Bacteria_large':'SpineLength > 3.5'}\n",
    "\n",
    "data = DatasetPool(path=path, dsList=dsList, filters=filters)\n",
    "\n",
    "data.describe(['median', 'min', 'max'], include='SpineLength')"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note that when filtering a parent table, all child tables will be filtered accordingly.\n",
    "\n",
    "For example, when we filtered \"Bacteria_large\", all spots contained in bacteria that were filtered out were removed as well.\n",
    "\n",
    "### Filtering a parent table based on a child table\n",
    "\n",
    "By default, filters are only propagated \"down\", e.g. if a bacteria is removed, all the spots it contains will be removed too, but removing a spot does not remove the bacteria.\n",
    "\n",
    "It is however possible to obtain this behaviour using the `filter_parent` method. This can be useful for example to select only cells where two fluorescent markers colocalise.\n",
    "\n",
    "In the cell below, we filter to keep only bacteria that contain a \"centered\" spot (defined as located between -1/2 and +1/2 of the cell length).\n",
    "\n",
    "Note the reduced number of objects (cells) after filtering the parent table."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "filters = {'Spot_detection': 'SpineCurvilinearCoord > 0.25*SpineLength and SpineCurvilinearCoord < 0.75*SpineLength'}\n",
    "\n",
    "data = DatasetPool(path=path, dsList=dsList, filters=filters)\n",
    "\n",
    "filtered_data = data.filter_parent(object_name='Spot_detection')\n",
    "\n",
    "print('\\nBefore filtering:')\n",
    "data.describe(['min', 'max'], include=['SpineLength', 'SpineCurvilinearCoord'], object_name='Bacteria_large')\n",
    "print('After filtering:')\n",
    "filtered_data.describe(['min', 'max'], include=['SpineLength', 'SpineCurvilinearCoord'], object_name='Bacteria_large')"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Plotting\n",
    "\n",
    "Seaborn's object interface allows to easily and flexibly plot data.\n",
    "\n",
    "💡 Tip: replace `so.Area()` by `so.Bars()` to plot histogram bars."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import seaborn.objects as so\n",
    "\n",
    "data = DatasetPool(path=path, dsList=dsList, filters={})\n",
    "\n",
    "(\n",
    "    so.Plot(data.Bacteria_large, x='SpineLength', color='Group')\n",
    "    .add(so.Area(), so.Hist(binwidth=.5, stat='probability', common_norm=False))\n",
    "    .scale(color='deep')\n",
    "    .limit(x=(None, None), y=(None, None))\n",
    "    .label(x=\"Cell length (µm)\", y=\"Probability\", title='', color='')\n",
    ")"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Alternatively, you can use one of PyBerries pre-set plots.\n",
    "\n",
    "You can find examples of plot presets in the [plot_preset documentation](https://gitlab.com/MEKlab/pyberries/-/blob/main/doc/DatasetPool.plot_preset.md) and the [plot_preset gallery](https://gitlab.com/MEKlab/pyberries/-/blob/main/doc/Plot_preset_gallery.md)."
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Grouping experiments together\n",
    "\n",
    "In our example datasets, there are two experimental groups:\n",
    "- Datasets 2 and 4 had an antibiotic added before imaging\n",
    "- Datasets 1 and 3 were controls (no antibiotic added)\n",
    "\n",
    "We can reflect this in our graphs by using the `groups` parameter:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "groups = ['No antibiotic', '+ antibiotic', 'No antibiotic', '+ antibiotic']\n",
    "\n",
    "data = DatasetPool(path=path, dsList=dsList, filters={}, groups=groups)"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note that when groups are provided, there should be one per dataset in `dsList`.\n",
    "\n",
    "Now if we plot the same histogram as above, datasets from the same group are added together:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "(\n",
    "    so.Plot(data.Bacteria_large, x='SpineLength', color='Group')\n",
    "    .add(so.Area(), so.Hist(binwidth=.5, stat='probability', common_norm=False))\n",
    "    .scale(color='deep')\n",
    "    .limit(x=(None, None), y=(None, None))\n",
    "    .label(x=\"Cell length (µm)\", y=\"Probability\", title='', color='')\n",
    ")"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To plot error bars computed from experimental replicates, we must first compute the histogram using get_histogram.\n",
    "\n",
    "In the plot definition, we add the following elements:\n",
    "- `so.Agg()`: gives to each bar the average value between datasets of the same group\n",
    "- `so.Dodge()`: show bars next to each other rather than overlapped\n",
    "- `so.Range()` and `so.Est(errorbar='sd')` to display the standard deviation as errorbars\n",
    "    - For more information on the types of error bars available, see [Seaborn's errorbar tutorial](https://seaborn.pydata.org/tutorial/error_bars.html)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from pyberries.data import get_histogram\n",
    "\n",
    "hist = get_histogram(data.Bacteria_large, col='SpotCount', groupby=['Group', 'Dataset'], density=True, discrete=True)\n",
    "\n",
    "(\n",
    "    so.Plot(hist, x='SpotCount', y='proportion', color='Group')\n",
    "    .add(so.Bar(), so.Agg(), so.Dodge())\n",
    "    .add(so.Range(), so.Est(errorbar='sd'), so.Dodge(), legend=False)\n",
    "    .scale(color='deep')\n",
    "    .limit(x=(None, None), y=(0, None))\n",
    "    .label(x=\"Number of spots\", y=\"Probability\", title='', color='')\n",
    ")"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Other resources\n",
    "\n",
    "This was an introduction to data import and plotting in PyBerries.\n",
    "\n",
    "For more information, I invite you to:\n",
    "- Read the [DatasetPool documentation](https://gitlab.com/MEKlab/pyberries/-/blob/main/doc/DatasetPool/DatasetPool.md?ref_type=heads)\n",
    "- Read the [PyBerries main functionalities](https://gitlab.com/MEKlab/pyberries/-/blob/main/doc/PyBerries_main_functionalities.md) which summarises what you have learned here and goes into more details about the different methods for data manipulation and plotting\n",
    "- Try the [data fitting tutorial](https://gitlab.com/MEKlab/pyberries/-/blob/main/Tutorial/Fitting_Tutorial.ipynb) to fit different types of data in PyBerries\n",
    "- Try the [statistical tests tutorial](https://gitlab.com/MEKlab/pyberries/-/blob/main/Tutorial/Stats_tutorial.ipynb) to find statistically significant differences in data\n",
    "- Check out the [plot_preset documentation](https://gitlab.com/MEKlab/pyberries/-/blob/main/doc/DatasetPool/DatasetPool.plot_preset.md) and [Seaborn documentation](https://seaborn.pydata.org/index.html) for more plot ideas"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "pyberries_py3_13",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.13.0"
  },
  "orig_nbformat": 4
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
