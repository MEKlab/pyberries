{
 "cells": [
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Fitting Tutorial\n",
    "\n",
    "This notebook will demonstrate how to use PyBerries to import data, fit it with custom functions, and display the fit parameters.\n",
    "\n",
    "First, install PyBerries:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%pip install PyBerries==0.2.18"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Download and unzip the [Example fitting datasets](https://gitlab.com/MEKlab/pyberries/-/raw/main/Tutorial/Example_fitting_data.zip?inline=false)\n",
    "\n",
    "The Datasets contain segmented bacteria and segmented spots, recorded in a timelapse experiment. There are two different conditions: \"cipro0\" (control) and \"cipro30ngmL\" (exposure to antibiotic during imaging).\n",
    "\n",
    "During this tutorial, we will fit:\n",
    "- The rate of disappearance of the spots\n",
    "- The bleaching rate of the total fluorescence in the cells\n",
    "- The cell length vs. cell width relationship (custom fitting function)\n",
    "\n",
    "Update the path to the `Example_fitting_data` folder you downloaded (use `\\\\` instead of `\\` on Windows):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "path = 'C:\\\\Users\\\\dthedie\\\\Downloads\\\\Example_fitting_data'"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Importing and filtering the data\n",
    "\n",
    "Upon import, we apply to operations:\n",
    "- A filter on `TrackLength`, to remove spots that were tracked for only one frame (which are likely to contain many false positives)\n",
    "- On the `Spot_detection` table, we remove duplicated rows according to the `Dataset`, `Position` and `TrackHeadIndices` columns. This is because our `Spot_detection` table contains one line per detected spot; and for each spot the `TrackLength` column shows how many spots there are in its track. If we did not remove duplicates, we would count a track of 2 spots twice, a track of 3 spots three times, etc..."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from pyberries.data import DatasetPool\n",
    "\n",
    "dataset_list = ['230126_cipro0', '230426_cipro0', '230426_cipro30ngmL', '230201_cipro30ngmL']\n",
    "groups = ['No cipro', 'No cipro', 'Cipro 30 ng/mL', 'Cipro 30 ng/mL']\n",
    "\n",
    "data = (DatasetPool(path=path, dsList=dataset_list, groups=groups, filters={'Spot_detection':'TrackLength > 1'})\n",
    "        .drop_duplicates(object_name='Spot_detection', subset=['Dataset', 'Position', 'TrackHeadIndices'])\n",
    "        )"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Fitting the spot disappearance rate\n",
    "\n",
    "Lifetime of the fluorescent spots is represented in our data by the `TrackLength` property.\n",
    "- Since we removed duplicates, \"nObjects\" corresponds to the number of tracks"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "data.describe(['mean', 'std'], object_name='Spot_detection', include='TrackLength')"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "By fitting an exponential decay (`y = a*exp(-b*x)`) to the TrackLength histogram, we can obtain the spot disappearance rate (b).\n",
    "\n",
    "To do this, we generate the TrackLength histogram using the `get_histogram` function with the following arguments:\n",
    "- `density=True` normalises histogram bars by the total number of observations\n",
    "- `groupby=['Dataset','Group']` specifies that we fit Datasets individually; we also include 'Group' to keep that information for following plots"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from pyberries.data import get_histogram\n",
    "\n",
    "hist = get_histogram(data.Spot_detection, col='TrackLength', binsize=2, groupby=['Dataset','Group'], density=True)\n",
    "display(hist.head())"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Next we create a `Fit` object.\n",
    "- `model_type='monoexp_decay'` is a preset fitting function. For a complete list, see the [Fit documentation](https://gitlab.com/MEKlab/pyberries/-/blob/main/doc/Fit/Fit.md)\n",
    "- `p0` gives initial values for the fit parameters"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from pyberries.data import Fit\n",
    "\n",
    "lifetime_fit = Fit(hist, x='TrackLength', y='proportion', model_type='monoexp_decay',\n",
    "                   groupby=['Dataset','Group'], p0=[hist.TrackLength.iloc[0], 1])"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can now look at the fit results, contained in the `data` attribute of the `Fit` object."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import seaborn.objects as so\n",
    "\n",
    "(\n",
    "    so.Plot(lifetime_fit.data, x='TrackLength', color='Dataset')\n",
    "    .facet(col='Dataset', wrap=2)\n",
    "    .add(so.Bars(), y='proportion', legend=False)\n",
    "    .add(so.Line(), y='Fit', legend=False)\n",
    "    .scale(color='deep')\n",
    "    .limit(x=(0, 50), y=(None, None))\n",
    "    .label(x=\"Spot lifetime (frames)\", y=\"Probability\", color='')\n",
    ")"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Even though these fits look good, a closer look at the tail of the histogram reveals that we are not fitting it very well:\n",
    "- Notice the adjusted Y limit on the \"limit\" line"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "(\n",
    "    so.Plot(lifetime_fit.data, x='TrackLength', color='Dataset')\n",
    "    .facet(col='Dataset', wrap=2)\n",
    "    .add(so.Bars(), y='proportion', legend=False)\n",
    "    .add(so.Line(), y='Fit', legend=False)\n",
    "    .scale(color='deep')\n",
    "    .limit(x=(0, 50), y=(None, 0.05))\n",
    "    .label(x=\"Spot lifetime (frames)\", y=\"Probability\", color='')\n",
    ")"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This poor fit could be due to to distinct sub-populations of fluorescent spots, disappearing with different rates.\n",
    "\n",
    "We can take this into account by fitting a \"bi-exponential decay\": `y = a1*exp(-b1*x) + a2*exp(-b2*x)`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "lifetime_fit = Fit(hist, x='TrackLength', y='proportion', model_type='biexp_decay',\n",
    "                   groupby=['Dataset','Group'], p0=[hist.TrackLength.iloc[0], 1, hist.TrackLength.iloc[0]/10, 0.1])\n",
    "\n",
    "(\n",
    "    so.Plot(lifetime_fit.data, x='TrackLength', color='Dataset')\n",
    "    .facet(col='Dataset', wrap=2)\n",
    "    .add(so.Bars(), y='proportion', legend=False)\n",
    "    .add(so.Line(), y='Fit', legend=False)\n",
    "    .scale(color='deep')\n",
    "    .limit(x=(0, 50), y=(None, 0.05))\n",
    "    .label(x=\"Spot lifetime (frames)\", y=\"Probability\", color='')\n",
    ")"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This model seems to fit our experimental data much better, by taking into account a population of molecules disappearing with a \"fast\" rate (b1) and one disappearing with a \"slow\" rate (b2).\n",
    "\n",
    "We can take a closer look at the fitted rates using the `get_rates` method of `Fit`:\n",
    "- `dt` is the time interval between frames, in seconds\n",
    "- Note that we are now plotting per \"Group\" to compute the average rate with/without antibiotic.\n",
    "\n",
    "You can further customise the plot, for example:\n",
    "- Replace `so.Dash(width=.3)` with `so.Dot()` to display the average value as a dot\n",
    "- Add the line `.add(so.Range(), so.Est(errorbar='sd'), legend=False)` to add errorbars corresponding to the standard deviation"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "rates = lifetime_fit.get_rates(dt=2)\n",
    "\n",
    "(\n",
    "    so.Plot(rates, x='Group', y='Rate', color='Group')\n",
    "    .facet(row='Rate_type')\n",
    "    .add(so.Dots(), so.Jitter(), legend=False)\n",
    "    .add(so.Dash(width=.3), so.Agg('mean'), legend=False)\n",
    "    .scale(color='deep')\n",
    "    .share(y=False)\n",
    "    .limit(y=(None, None))\n",
    "    .label(x=\"\", y=\"\", color='', title=\"{} rate\".format)\n",
    ")"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Finally, we can plot the proportion of spots in the fast vs. slow disappearing populations:\n",
    "- The black bar represents standard deviation"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "(\n",
    "    so.Plot(data=rates, x='Population', y='Group', color='Rate_type')\n",
    "    .add(so.Bars(), so.Agg(), so.Stack())\n",
    "    .add(so.Range(linewidth=2, color='0'), so.Est(errorbar=\"sd\"), data=rates.query('Rate_type == \"Fast\"'), legend=False)\n",
    "    .scale(color='deep')\n",
    "    .limit(x=(0, 1), y=(None, None))\n",
    "    .label(x=\"Population (fraction)\", y=\"\", title='', color='')\n",
    ")"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Fitting the fluorescence bleaching rate\n",
    "\n",
    "The background-subtracted fluorescence intensity inside the cells is stored in the `SNR` column of our `Bacteria_large` table.\n",
    "- In this table, nObjects corresponds to the number of segmented cells"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "data.describe(['mean', 'std'], object_name='Bacteria_large', include='SNR')"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Fitting the decrease in fluorescence intensity with an exponential decay gives us the fluorescence bleaching rate.\n",
    "\n",
    "Here, we will use a \"mono-exponential decay with offset\": `y = a*exp(-b*x) + c`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from pyberries.data import Fit\n",
    "\n",
    "bleaching_fit = Fit(data.Bacteria_large, x='Frame', y='SNR', groupby='Group', model_type='monoexp_decay_offset', p0=[data.Bacteria_large.SNR.iloc[0], 1, 0])"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As before, we can then look at the fit quality:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import seaborn.objects as so\n",
    "\n",
    "(\n",
    "    so.Plot(bleaching_fit.data, x='Frame', color='Group')\n",
    "    .layout(size=(12, 4))\n",
    "    .facet(col='Group')\n",
    "    .add(so.Line(alpha=.8), so.Agg(), y='SNR', legend=False)\n",
    "    .add(so.Line(linestyle='--'), y='Fit', legend=False)\n",
    "    .scale(color='deep')\n",
    "    .share(x=False, y=False)\n",
    "    .limit(x=(None, None), y=(None, None))\n",
    "    .label(x=\"Illumination time (sec)\", y=\"Fluorescence (AU)\", color='')\n",
    ")"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can use the `get_fit_parameters` method of `Fit` to retrieve the values of our fit parameters: amplitude (a), rate (b) and offset (c)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "display(fit_param = bleaching_fit.get_fit_parameters(param_names=['Amplitude', 'Rate', 'Offset']))"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can also fit the bleaching rate per Position, for each Dataset (this will take a bit longer to run):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "bleaching_fit = Fit(data.Bacteria_large, x='Frame', y='SNR', groupby=['Group', 'Dataset', 'Position'], model_type='monoexp_decay_offset', p0=[data.Bacteria_large.SNR.iloc[0], 1, 0])\n",
    "\n",
    "(\n",
    "    so.Plot(bleaching_fit.data, x='Frame', color='Position')\n",
    "    .layout(size=(12, 4))\n",
    "    .facet(col='Dataset')\n",
    "    .add(so.Line(alpha=.8), so.Agg(), y='SNR', legend=False)\n",
    "    .add(so.Band(), so.Est(errorbar='se'), y='SNR', legend=False)\n",
    "    .add(so.Line(linestyle='--'), y='Fit', legend=False)\n",
    "    .scale(color='deep')\n",
    "    .share(x=False, y=False)\n",
    "    .limit(x=(None, None), y=(None, None))\n",
    "    .label(x=\"Illumination time (sec)\", y=\"Fluorescence (AU)\", color='')\n",
    ")"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Plot the parameters as individual points (lines shows the median values):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fit_param = bleaching_fit.get_fit_parameters(param_names=['Amplitude', 'Rate', 'Offset'])\n",
    "\n",
    "(\n",
    "    so.Plot(fit_param, x='Dataset', y='Fitted_param', color='Dataset')\n",
    "    .facet(row='Param_name')\n",
    "    .add(so.Dots(), so.Jitter(.25), legend=False)\n",
    "    .add(so.Dash(width=.3), so.Agg('median'), legend=False)\n",
    "    .scale(color='deep')\n",
    "    .share(y=False)\n",
    "    .limit(x=(None, None), y=(None, None))\n",
    "    .label(x=\"\", y=\"\", color='')\n",
    ")"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Fitting an arbitrary function\n",
    "\n",
    "It is also possible to define a custom fitting function to use with Fit.\n",
    "\n",
    "For example, we can try to fit the SpineLength vs. SpineWidth data with a straight line:\n",
    "- Define a model function that will be used for fitting\n",
    "- Pass the function to Fit in the `model` argument instead of providing `model_type`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from pyberries.data import Fit\n",
    "\n",
    "def model(x, a, b):\n",
    "    return a*x + b\n",
    "cell_dim_fit = Fit(data.Bacteria_large, x='SpineLength', y='SpineWidth', model=model, groupby='Dataset')"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Show the fit results:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import seaborn.objects as so\n",
    "\n",
    "(\n",
    "    so.Plot(cell_dim_fit.data, x='SpineLength', color='Dataset')\n",
    "    .facet(col='Dataset', wrap=2)\n",
    "    .add(so.Dots(pointsize=2), y='SpineWidth', legend=False)\n",
    "    .add(so.Line(color='.2'), y='Fit', legend=False)\n",
    "    .scale(color='deep')\n",
    "    .share(x=False, y=False)\n",
    "    .limit(x=(None, None), y=(None, None))\n",
    "    .label(x=\"Cell length (µm)\", y=\"Cell width (µm)\", color='')\n",
    ")"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Display fit parameters:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "display(cell_dim_fit.get_fit_parameters(param_names=['Slope', 'Offset']))"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "test",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.13"
  },
  "orig_nbformat": 4
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
